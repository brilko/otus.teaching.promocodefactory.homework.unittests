﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using static Otus.Teaching.PromoCodeFactory.Services.Interfaces.IPartnerService;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    public class PartnerService : IPartnerService
    {
        private readonly IRepository<Partner> partnersRepository;

        public PartnerService(IRepository<Partner> partnersRepository)
        {
            this.partnersRepository = partnersRepository;
        }

        public async Task<(SetPartnerPromoCodeLimitAnswers, Guid)> SetPartnerPromoCodeLimitAsync(
            Guid id, SetPartnerPromoCodeLimitRequestDto request)
        {

            var partner = await partnersRepository.GetByIdAsync(id);

            if (partner == null)
                return (SetPartnerPromoCodeLimitAnswers.PartnerNotFound, Guid.Empty);

            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                return (SetPartnerPromoCodeLimitAnswers.PartnerIsNotActive, Guid.Empty);

            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов,
                //которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }

            if (request.Limit <= 0)
                return (SetPartnerPromoCodeLimitAnswers.LimitIsZero, Guid.Empty);

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = request.EndDate
            };

            partner.PartnerLimits.Add(newLimit);

            await partnersRepository.UpdateAsync(partner);

            return (SetPartnerPromoCodeLimitAnswers.Setted, newLimit.Id);
        }
    }
}
