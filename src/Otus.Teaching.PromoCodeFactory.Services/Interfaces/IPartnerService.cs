﻿using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Interfaces
{
    public interface IPartnerService
    {
        Task<(SetPartnerPromoCodeLimitAnswers, Guid)> SetPartnerPromoCodeLimitAsync(
            Guid id, SetPartnerPromoCodeLimitRequestDto request);

        enum SetPartnerPromoCodeLimitAnswers
        {
            PartnerNotFound,
            PartnerIsNotActive,
            LimitIsZero,
            Setted
        }
    }
}
