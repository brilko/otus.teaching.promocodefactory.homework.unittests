﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Services.Contracts
{
    public class SetPartnerPromoCodeLimitRequestDto
    {
        public DateTime EndDate { get; set; }
        public int Limit { get; set; }
    }
}
