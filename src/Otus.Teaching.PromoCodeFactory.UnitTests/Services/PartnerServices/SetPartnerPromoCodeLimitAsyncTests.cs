﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Services.Implementations;
using Otus.Teaching.PromoCodeFactory.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Services.PartnerServices
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> partnersRepositoryMock;
        private readonly PartnerService partnersService;
        private readonly IFixture fixture;
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
            partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            partnersService = fixture
                .Build<PartnerService>()
                .Create();
        }

        private async Task<(Partner, PartnerPromoCodeLimit, SetPartnerPromoCodeLimitRequestDto,
            IPartnerService.SetPartnerPromoCodeLimitAnswers)> ArrangeAndActWithActiveLimitAsync()
        {
            //Arrange
            var partner = fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .Without(p => p.PartnerLimits)
                .Create();

            var limit = fixture.Build<PartnerPromoCodeLimit>()
                    .Without(l => l.CancelDate)
                    .With(l => l.PartnerId, partner.Id)
                    .Without(l => l.Partner)
                    .Create();

            partner.PartnerLimits = new List<PartnerPromoCodeLimit>()
            {
                limit
            };

            partnersRepositoryMock
                .Setup(r => r.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var request = fixture.Build<SetPartnerPromoCodeLimitRequestDto>()
                .With(r => r.EndDate, DateTime.Now.AddDays(10))
                .Create();

            //Act
            var (actionResult, _) = await partnersService
                .SetPartnerPromoCodeLimitAsync(partner.Id, request);

            return (partner, limit, request, actionResult);
        }

        //Если партнер не найден, то также нужно выдать ошибку 404;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            //Arrange
            var partnerId = fixture.Create<Guid>();
            Partner notFoundPartner = null;

            partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(notFoundPartner);

            //Act
            var (actionResult, _) = await partnersService
                .SetPartnerPromoCodeLimitAsync(partnerId, null);

            //Assert
            actionResult.Should()
                .HaveSameValueAs(IPartnerService.SetPartnerPromoCodeLimitAnswers.PartnerNotFound);
        }


        //Если партнер заблокирован, то есть поле IsActive = false в классе Partner,
        //то также нужно выдать ошибку 400;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsNotFound()
        {
            //Arrange
            var partner = fixture.Build<Partner>()
                .With(p => p.IsActive, false)
                .Without(p => p.PartnerLimits)
                .Create();

            partnersRepositoryMock
                .Setup(r => r.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            var (actionResult, _) = await partnersService
                .SetPartnerPromoCodeLimitAsync(partner.Id, null);

            //Assert
            actionResult.Should()
                .HaveSameValueAs(IPartnerService.SetPartnerPromoCodeLimitAnswers.PartnerIsNotActive);
        }

        //Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        //которые партнер выдал NumberIssuedPromoCodes,
        //если лимит закончился, то количество не обнуляется;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasActiveLimit_PromocodesResetToZero()
        {
            // Arrange and Act
            (var partner, var limit, _, _) = await ArrangeAndActWithActiveLimitAsync();

            //Assert
            partner.Should()
                .Match(p => ((Partner)p).NumberIssuedPromoCodes == 0);
            limit.CancelDate.Should()
                .BeSameDateAs(DateTime.Now);
        }

        //TODO: При установке лимита нужно отключить предыдущий лимит;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasActiveLimit_DeactiveLastLimit()
        {
            // Arrange and Act
            (_, var limit, _, _) = await ArrangeAndActWithActiveLimitAsync();

            //Assert
            limit.CancelDate.Should()
                .NotBeNull();
        }

        //Лимит должен быть больше 0;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ZeroLimit_ReturnsBadRequest()
        {
            //Arrange
            var partner = fixture.Build<Partner>()
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .With(p => p.IsActive, true)
                .Create();

            partnersRepositoryMock
                .Setup(r => r.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var request = fixture.Build<SetPartnerPromoCodeLimitRequestDto>()
                .With(r => r.Limit, 0)
                .Create();

            //Act
            var (actionResult, _) = await partnersService
                .SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            actionResult.Should()
                .HaveSameValueAs(IPartnerService.SetPartnerPromoCodeLimitAnswers.LimitIsZero);
        }

        //Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SavingNewLimit_NewLimitSaved()
        {
            // Arrange and Act
            (var partner, _, var requestLimit, _) = await ArrangeAndActWithActiveLimitAsync();

            //Assert
            var rl = requestLimit;
            partner.PartnerLimits
                .Where(l => l.EndDate == rl.EndDate && l.Limit == rl.Limit)
                .FirstOrDefault()
                .Should()
                .NotBeNull();
        }
    }
}
