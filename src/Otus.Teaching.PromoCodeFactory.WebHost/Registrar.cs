﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Services.Implementations;
using Otus.Teaching.PromoCodeFactory.Services.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public static class Registrar
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection services)
        {
            return services
                .ConfigureDataBase()
                .ConfigureServices()
                .ConfigureMappingProfiles();
        }

        private static IServiceCollection ConfigureDataBase(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddDbContext<DataContext>(x =>
            {
                x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
                //x.UseNpgsql(Configuration.GetConnectionString("PromoCodeFactoryDb"));
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });
            return services;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            return services
                .AddScoped<IPartnerService, PartnerService>();
        }

        private static IServiceCollection ConfigureMappingProfiles(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<SetPartnerPromoCodeLimitRequestProfile>();
            });
            configuration.AssertConfigurationIsValid();

            return services
                .AddSingleton<IMapper>(new Mapper(configuration));
        }
    }
}
