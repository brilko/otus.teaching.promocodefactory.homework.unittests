﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class SetPartnerPromoCodeLimitRequestProfile : Profile
    {
        public SetPartnerPromoCodeLimitRequestProfile()
        {
            CreateMap<SetPartnerPromoCodeLimitRequest, SetPartnerPromoCodeLimitRequestDto>();
        }
    }
}
